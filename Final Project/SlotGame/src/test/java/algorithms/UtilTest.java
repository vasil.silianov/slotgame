package algorithms;

import config.ReadFromFile;
import constants.Constants;
import org.junit.jupiter.api.*;

import static constants.Constants.REEL_CONFIG_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

public class UtilTest {
    private ReadFromFile reelConfig; //= new ReadFromFile();
    private ArrayList<ArrayList<Integer>> allReels;
    private int positionInReelForTesting;

    public void setUp() {
        Util.loadProperties();
        reelConfig = new ReadFromFile();
        allReels = reelConfig.read(REEL_CONFIG_PATH);
    }

    @Test
    public void maxAvailableBetPerLine_WITH_INTEGER_NUMBERS_Test() {
        BigDecimal currentBalance = new BigDecimal(21);
        int numberOfLines = 3;
        BigDecimal result = Util.maxAvailableBetPerLine(currentBalance, numberOfLines);
        Assertions.assertEquals(new BigDecimal("7.00"), result);
    }

    @Test
    public void maxAvailableBetPerLine_WITH_DECIMAL_NUMBERS_Test() {
        BigDecimal currentBalance = new BigDecimal(21);
        int numberOfLines = 4;
        BigDecimal result = Util.maxAvailableBetPerLine(currentBalance, numberOfLines);
        Assertions.assertEquals(new BigDecimal("5.25"), result);
    }

    @Test
    public void maxAvailableBetPerLine_WITH_DECIMALx2_NUMBERS_Test() {
        BigDecimal currentBalance = new BigDecimal(21);
        int numberOfLines = 9;
        BigDecimal result = Util.maxAvailableBetPerLine(currentBalance, numberOfLines);
        Assertions.assertEquals(new BigDecimal("2.33"), result);
    }

    @Test
    public void maxAvailableBetPerLine_WITH_DECIMALx3_NUMBERS_Test() {
        BigDecimal currentBalance = new BigDecimal("21.71");
        int numberOfLines = 9;
        BigDecimal result = Util.maxAvailableBetPerLine(currentBalance, numberOfLines);
        Assertions.assertEquals(new BigDecimal("2.41"), result);
    }

    @Test
    public void currentSlotScreen_TEST_BEGGING_OF_REEL() {
        setUp();
        positionInReelForTesting = 0;
        int[] generatedRandomNumbers_Test1 = {positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting};
        int[][] slotScreen = Util.currentSlotScreen(allReels, generatedRandomNumbers_Test1);

        int[][] slotScreenToCompare = new int[3][allReels.size()];
        for (int i = 0; i < slotScreenToCompare.length; i++) {
            for (int j = 0; j < slotScreenToCompare[i].length; j++) {
                slotScreenToCompare[i][j] = allReels.get(j).get(i);
            }
        }


//        Util.print2DIntArr(slotScreen);
//        System.out.println();
//        Util.print2DIntArr(slotScreenToCompare);
        Assertions.assertArrayEquals(slotScreenToCompare, slotScreen);

    }

    @Test
    public void currentSlotScreen_TEST_MIDLE_OF_REEL() {
        setUp();
        positionInReelForTesting = allReels.get(0).size() / 2;
        int[] generatedRandomNumbers_Test1 = {positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting};
        int[][] slotScreen = Util.currentSlotScreen(allReels, generatedRandomNumbers_Test1);

        int[][] slotScreenToCompare = new int[3][allReels.size()];
        for (int i = 0; i < slotScreenToCompare.length; i++) {
            for (int j = 0; j < slotScreenToCompare[i].length; j++) {
                slotScreenToCompare[i][j] = allReels.get(j).get(positionInReelForTesting + i);
            }
        }

//                Util.print2DIntArr(slotScreen);
//        System.out.println();
//        Util.print2DIntArr(slotScreenToCompare);
//        System.out.println();
//        System.out.println(positionInReelForTesting);
        Assertions.assertArrayEquals(slotScreenToCompare, slotScreen);

    }

    @Test
    public void currentSlotScreen_TEST_SECOND_TO_LAST_POSITION_OF_REEL() {
        setUp();
        positionInReelForTesting = allReels.get(0).size() - 2;
        int[] generatedRandomNumbers_Test1 = {positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting};
        int[][] slotScreen = Util.currentSlotScreen(allReels, generatedRandomNumbers_Test1);

        int[][] slotScreenToCompare = new int[3][allReels.size()];
        for (int i = 0; i < slotScreenToCompare.length; i++) {
            for (int j = 0; j < slotScreenToCompare[i].length; j++) {
                if (positionInReelForTesting + i == allReels.get(i).size()) {
                    positionInReelForTesting = 0;
                    slotScreenToCompare[i][j] = allReels.get(j).get(positionInReelForTesting);
                }
                slotScreenToCompare[i][j] = allReels.get(j).get(positionInReelForTesting + i);
            }
        }

//                Util.print2DIntArr(slotScreen);
//        System.out.println();
//        Util.print2DIntArr(slotScreenToCompare);
//        System.out.println();
//        System.out.println(positionInReelForTesting);
        Assertions.assertArrayEquals(slotScreenToCompare, slotScreen);

    }


    @Test
    public void currentSlotScreen_TEST_LAST_POSITION_OF_REEL() {
        setUp();
        positionInReelForTesting = allReels.get(0).size() - 1;
        int[] generatedRandomNumbers_Test1 = {positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting, positionInReelForTesting};
        int[][] slotScreen = Util.currentSlotScreen(allReels, generatedRandomNumbers_Test1);

        int[][] slotScreenToCompare = new int[3][allReels.size()];
        for (int i = 0; i < slotScreenToCompare.length; i++) {
            for (int j = 0; j < slotScreenToCompare[i].length; j++) {
                if (positionInReelForTesting + i == allReels.get(i).size()) {
                    positionInReelForTesting = i - 1;
                    slotScreenToCompare[i][j] = allReels.get(j).get(positionInReelForTesting);
                }
                slotScreenToCompare[i][j] = allReels.get(j).get(positionInReelForTesting + i);
            }
        }

//        Util.print2DIntArr(slotScreen);
//        System.out.println();
//        Util.print2DIntArr(slotScreenToCompare);
//        System.out.println();
//        System.out.println(positionInReelForTesting);
        Assertions.assertArrayEquals(slotScreenToCompare, slotScreen);

    }


    @Test
    public void returnAllRandomNumbers_TEST() {
        boolean isInRange;// = false;
        setUp();
        for (int j = 0; j <500; j++) {

            int[] randomNumbersForReel = Util.returnAllRandomNumbers(allReels);

            for (int i = 0; i < randomNumbersForReel.length; i++) {
                isInRange = false;
                if (randomNumbersForReel[i] < allReels.get(i).size()) {
                    isInRange = true;
                }
                Assertions.assertTrue(isInRange);
            }
        }
    }

    /*
        public static int[] returnAllRandomNumbers(ArrayList<ArrayList<Integer>> allReels){

        int[] resultRandomNumbers = new int[allReels.size()];

        for (int i = 0; i < allReels.size(); i++) {

            resultRandomNumbers[i] = random.nextInt(allReels.get(i).size());
        }

        return  resultRandomNumbers;
    }
     */

}
