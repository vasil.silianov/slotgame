package algorithms;

import config.ReadFromFile;
import constants.Constants;
import model.WinCharacteristics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CheckScatterTest {

    private CheckScatter checkScatter;
    private ReadFromFile cardsPaytableConfig;

    public void setUp() {
        Util.loadProperties();
        cardsPaytableConfig = new ReadFromFile();
        checkScatter = new CheckScatter(cardsPaytableConfig.read(Constants.PAY_TABLE_CONFIG_PATH));
    }


    @Test
    public void calculateScatterWinsWITH_TWO_SCATERS_Test() {
        setUp();
        WinCharacteristics scatterWin = checkScatter.calculateScatterWins(slotScreen0, totalBet);
        Assertions.assertEquals(new BigDecimal(0), scatterWin.getWinningAmount());
    }
    @Test
    public void calculateScatterWinsWITH_THREE_SCATERS_Test() {
        setUp();
        WinCharacteristics scatterWin = checkScatter.calculateScatterWins(slotScreen1, totalBet);
        Assertions.assertEquals(new BigDecimal(100), scatterWin.getWinningAmount());
    }

    @Test
    public void calculateScatterWinsWITH_FOUR_SCATERS_Test() {
        setUp();
        WinCharacteristics scatterWin = checkScatter.calculateScatterWins(slotScreen2, totalBet);
        Assertions.assertEquals(new BigDecimal(400), scatterWin.getWinningAmount());
    }

    @Test
    public void calculateScatterWinsWITH_FIVE_SCATERS_Test() {
        setUp();
        WinCharacteristics scatterWin = checkScatter.calculateScatterWins(slotScreen3, totalBet);
        Assertions.assertEquals(new BigDecimal(10000), scatterWin.getWinningAmount());
    }

    //Variables for testing purposes
    private static BigDecimal totalBet = new BigDecimal(20);

    private static int[][] slotScreen0 = {{1, 1, 0, 1, 5},
            {7, 1, 0, 1, 5},
            {4, 7, 1, 2, 0}};
    private static int[][] slotScreen1 = {{1, 1, 0, 1, 5},
            {7, 1, 0, 1, 5},
            {4, 7, 1, 7, 0}};

    private static int[][] slotScreen2 = {{1, 1, 7, 1, 5},
            {7, 1, 0, 1, 5},
            {4, 7, 1, 7, 0}};
    private static int[][] slotScreen3 = {{1, 1, 7, 1, 7},
            {7, 1, 0, 1, 5},
            {4, 7, 1, 7, 0}};

}
