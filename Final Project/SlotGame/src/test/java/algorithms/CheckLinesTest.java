package algorithms;

import config.ReadFromFile;
import model.WinCharacteristics;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;


import static constants.Constants.LINE_CONFIG_PATH;
import static constants.Constants.PAY_TABLE_CONFIG_PATH;

public class CheckLinesTest {


    ReadFromFile fileReader;
    ArrayList<ArrayList<Integer>> lineConfig;
    ArrayList<ArrayList<Integer>> payTable;
    CheckLines checkLines;


    private void setUp() {
        Util.loadProperties();
        fileReader = new ReadFromFile();
        lineConfig = fileReader.read(LINE_CONFIG_PATH);
        payTable = fileReader.read(PAY_TABLE_CONFIG_PATH);
        checkLines = new CheckLines(payTable, lineConfig);
    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_WITH_SCENARIO_WIN_ON_LINES_1_3_13_WITH_ONE_WILDCARD_STREAK_FOUR() {
        BigDecimal betPerLine = new BigDecimal(20);
        int numberOfLinesToCheck = 20;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINES_1_3_13_WITH_ONE_WILDCARD_STREAK_FOUR, numberOfLinesToCheck);


        Assertions.assertEquals(new BigDecimal(400), wins.get(0).getWinningAmount());
        Assertions.assertEquals(4, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());


        Assertions.assertEquals(new BigDecimal(400), wins.get(1).getWinningAmount());
        Assertions.assertEquals(4, wins.get(1).getWinningStreak());
        Assertions.assertEquals(3, wins.get(1).getWinningLine());


        Assertions.assertEquals(new BigDecimal(400), wins.get(2).getWinningAmount());
        Assertions.assertEquals(4, wins.get(2).getWinningStreak());
        Assertions.assertEquals(13, wins.get(2).getWinningLine());

    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINES_1_5_WITH_SIX_STREAK_TWO() {
        BigDecimal betPerLine = new BigDecimal(25);
        int numberOfLinesToCheck = 20;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINES_1_5_WITH_SIX_STREAK_TWO, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal(250), wins.get(0).getWinningAmount());
        Assertions.assertEquals(2, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());

        Assertions.assertEquals(new BigDecimal(250), wins.get(1).getWinningAmount());
        Assertions.assertEquals(2, wins.get(1).getWinningStreak());
        Assertions.assertEquals(5, wins.get(1).getWinningLine());
    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINES_1_5_13_STREAK_FOUR_NO_WILDCARD() {
        BigDecimal betPerLine = new BigDecimal("2.7");
        int numberOfLinesToCheck = 20;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINES_1_5_13_STREAK_FOUR_NO_WILDCARD, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("54.0"), wins.get(0).getWinningAmount());
        Assertions.assertEquals(4, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());

        Assertions.assertEquals(new BigDecimal("54.0"), wins.get(1).getWinningAmount());
        Assertions.assertEquals(4, wins.get(1).getWinningStreak());
        Assertions.assertEquals(3, wins.get(1).getWinningLine());

        Assertions.assertEquals(new BigDecimal("54.0"), wins.get(2).getWinningAmount());
        Assertions.assertEquals(4, wins.get(2).getWinningStreak());
        Assertions.assertEquals(13, wins.get(2).getWinningLine());
    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINE_20_WITH_ON_WILDCARD_STREAK_FIVE_CARD_FOUR() {
        BigDecimal betPerLine = new BigDecimal("3.9");
        int numberOfLinesToCheck = 20;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINE_20_WITH_ON_WILDCARD_STREAK_FIVE_CARD_FOUR, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("780.0"), wins.get(0).getWinningAmount());
        Assertions.assertEquals(5, wins.get(0).getWinningStreak());
        Assertions.assertEquals(19, wins.get(0).getWinningLine());

    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINE_1_TYPE_64445() {
        BigDecimal betPerLine = new BigDecimal("10");
        int numberOfLinesToCheck = 2;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINE_1_TYPE_64445, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("400"), wins.get(0).getWinningAmount());
        Assertions.assertEquals(4, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());

    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINE_1_TYPE_66615() {
        BigDecimal betPerLine = new BigDecimal("10");
        int numberOfLinesToCheck = 2;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINE_1_TYPE_66615, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("400"), wins.get(0).getWinningAmount());
        Assertions.assertEquals(6, wins.get(0).getWinningElement());
        Assertions.assertEquals(3, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());

    }

    @Test
    public void calculateBalanceForEachWinningLine_TEST_SCENARIO_WIN_ON_LINE_1_TYPE_66616() {
        BigDecimal betPerLine = new BigDecimal("4");
        int numberOfLinesToCheck = 2;
        setUp();
        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(betPerLine, SCENARIO_WIN_ON_LINE_1_TYPE_66616, numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("400"), wins.get(0).getWinningAmount());
        Assertions.assertEquals(1, wins.get(0).getWinningElement());
        Assertions.assertEquals(5, wins.get(0).getWinningStreak());
        Assertions.assertEquals(1, wins.get(0).getWinningLine());
    }

    // to test


    @Test
    public void totalAmountOfLineWins_Test_SCENARIO_WIN_ON_LINES_1_5_WITH_SIX_STREAK_TWO() {
        int[][] currentSlotScreen = SCENARIO_WIN_ON_LINES_1_5_WITH_SIX_STREAK_TWO;
        int numberOfLinesToCheck = 20;
        BigDecimal betPerLine = new BigDecimal("2.5");
        setUp();
        ArrayList<WinCharacteristics> wins =  checkLines.calculateBalanceForEachWinningLine(betPerLine,currentSlotScreen,numberOfLinesToCheck);

        Assertions.assertEquals(new BigDecimal("50.0"), checkLines.totalAmountOfLineWins(wins));

    }


    //Test Scenarios

    int[][] SCENARIO_WIN_ON_LINES_1_5_13_STREAK_FOUR_NO_WILDCARD = {{1, 1, 1, 1, 5},
            {7, 1, 0, 1, 5},
            {4, 0, 1, 7, 0}};

    int[][] SCENARIO_WIN_ON_LINES_1_5_WITH_SIX_STREAK_TWO = {{6, 6, 7, 1, 5},
            {7, 5, 7, 1, 5},
            {4, 0, 1, 7, 0}};

    final static int[][] SCENARIO_WIN_ON_LINES_1_3_13_WITH_ONE_WILDCARD_STREAK_FOUR = {{1, 6, 1, 1, 5},
            {7, 1, 0, 1, 5},
            {4, 0, 1, 7, 0}};

    final static int[][] SCENARIO_WIN_ON_LINE_20_WITH_ON_WILDCARD_STREAK_FIVE_CARD_FOUR = {{1, 4, 1, 4, 5},
            {4, 3, 0, 1, 4},
            {2, 0, 4, 7, 0}};
    final static int[][] SCENARIO_WIN_ON_LINE_1_TYPE_64445 = {{6, 4, 4, 4, 5},
            {4, 3, 0, 1, 4},
            {2, 0, 4, 7, 0}};
    final static int[][] SCENARIO_WIN_ON_LINE_1_TYPE_66615 = {{6, 6, 6, 1, 5},
            {4, 3, 0, 1, 4},
            {2, 0, 4, 7, 0}};
    final static int[][] SCENARIO_WIN_ON_LINE_1_TYPE_66616 = {{6, 6, 6, 1, 6},
            {4, 3, 0, 1, 4},
            {2, 0, 4, 7, 0}};

}
