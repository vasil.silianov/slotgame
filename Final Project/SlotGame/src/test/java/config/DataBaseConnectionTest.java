package config;

import algorithms.Util;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseConnectionTest {

    @InjectMocks
    private DataBaseConnection dataBaseConnection;
    @Mock
    private Connection mockConnection;
    @Mock
    private Statement mockStatement;

    @Before
    private void setUp() {
        Util.loadProperties();
        MockitoAnnotations.initMocks(this);
        dataBaseConnection = new DataBaseConnection(System.getProperty("db.url"), System.getProperty("db.user"), System.getProperty("db.password"));
    }

    @Test
    public void connectToDB_Test() throws SQLException {
        setUp();
        Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);
        Mockito.when(mockConnection.createStatement().executeUpdate(Mockito.any())).thenReturn(1);
        boolean value = dataBaseConnection.executeCRUDQuery("SELECT * FROM slot_game.game_statistics");
        Assertions.assertTrue(value);
        Mockito.verify(mockConnection.createStatement(), Mockito.times(1));
    }


}
