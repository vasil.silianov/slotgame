package constants;

import java.math.BigDecimal;

public class Constants {
    public static final String REEL_CONFIG_PATH = System.getProperty("gc.reel.config.path");
    public static final String LINE_CONFIG_PATH = System.getProperty("gc.line.config.path");
    public static final String PAY_TABLE_CONFIG_PATH = System.getProperty("gc.pay.table.config.path");
    public static final int MAX_AVAILABLE_LINES = Integer.parseInt(System.getProperty("gc.max.available.lines"));
    public static final int DISPOSITION_OF_PAY_TABLE_ARRAY = Integer.parseInt(System.getProperty("gc.disposition.of.pay.table.array"));
    public static final int WILDCARD = Integer.parseInt(System.getProperty("gc.wildcard"));
    public static final int SCATTER = Integer.parseInt(System.getProperty("gc.scatter"));
    public static final int NUMBER_OF_SLOT_SCREEN_ROWS = Integer.parseInt(System.getProperty("gc.screen.size"));
    public static final BigDecimal START_BALANCE = new BigDecimal(System.getProperty("gc.start.balance"));
    public static final BigDecimal MIN_BET_AMOUNT = new BigDecimal(System.getProperty("gc.min.bet.amount"));
    public static final int MIN_WIN_STREAK_WILD_CARD = Integer.parseInt(System.getProperty("gc.min.win.streak.wild.card"));
    public static final int MIN_WIN_STREAK_NORMAL_CARD = Integer.parseInt(System.getProperty("gc.min.win.streak.normal.card"));

}
