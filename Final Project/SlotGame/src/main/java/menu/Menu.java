package menu;

import model.GameCharacteristics;
import model.WinCharacteristics;

import java.math.BigDecimal;
import java.util.ArrayList;

public interface Menu {
    GameCharacteristics startMenu(BigDecimal balance, int maxLinesAvailable, BigDecimal maxBetsAvailable);
    void winsMenu(ArrayList<WinCharacteristics> allWinningLines, WinCharacteristics scatterWin);
    void noWinsMenu();
    void totalAmountMenu(BigDecimal totalAmountOfCurrentGame);
    GameCharacteristics endMenu(GameCharacteristics game);

}
