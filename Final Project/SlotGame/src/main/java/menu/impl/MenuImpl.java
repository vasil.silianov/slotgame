package menu.impl;

import menu.Menu;
import model.GameCharacteristics;
import model.WinCharacteristics;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Scanner;

import static constants.Constants.MIN_BET_AMOUNT;

public class MenuImpl implements Menu {
    private static Scanner input = new Scanner(System.in);


    public GameCharacteristics startMenu(BigDecimal balance, int maxLinesAvailable, BigDecimal maxBetsAvailable) {
        GameCharacteristics currentGame = new GameCharacteristics();
        System.out.printf("Balance: %.2f | Lines available: %d | Bets per all lines available: 1-%.2f\n", balance, maxLinesAvailable, maxBetsAvailable);
        System.out.printf("Please enter lines you want to play on and a bet per line:\n");
        System.out.printf("Enter lines you want to play:\n");
        int numberOfLinesPlayerWantsToBet = intInput(maxLinesAvailable);
        System.out.printf("Enter bet per line you want to play:\n");
        BigDecimal bigDecimalNumberOfLines = new BigDecimal(numberOfLinesPlayerWantsToBet);
        BigDecimal betPerLine = bigDecimalInput(balance.divide(bigDecimalNumberOfLines, 2, RoundingMode.DOWN));
        System.out.printf("you have chosen %d lines and %.2f bets per line\n", numberOfLinesPlayerWantsToBet, betPerLine);
        BigDecimal totalBet = betPerLine.multiply(new BigDecimal(numberOfLinesPlayerWantsToBet));
        BigDecimal balanceAfterBet = balance.subtract(totalBet);
        System.out.printf("Balance: %.2f\n", balanceAfterBet);
        currentGame.setBalance(balanceAfterBet);
        currentGame.setBetPerLine(betPerLine);
        currentGame.setNumberOfLinesToBet(numberOfLinesPlayerWantsToBet);
        currentGame.setTotalBet(totalBet);
        return currentGame;
    }

    public void winsMenu(ArrayList<WinCharacteristics> allWinningLines, WinCharacteristics scatterWin) {
        StringBuilder sb = new StringBuilder();
        for (WinCharacteristics win : allWinningLines) {
            sb.append("Line ").append(win.getWinningLine()).append(", ");
            sb.append("Card ").append(win.getWinningElement()).append("x").append(win.getWinningStreak()).append(", ");
            sb.append("win amount ").append(win.getWinningAmount()).append(System.lineSeparator());
        }
        if (scatterWin.getWinningLine() == -1) {
            sb.append("Scatters SEVEN x").append(scatterWin.getWinningStreak()).append(", win amount ").append(scatterWin.getWinningAmount());
        }

        System.out.println(sb);
    }

    public void noWinsMenu() {
        System.out.println("No wins");
    }

    public void totalAmountMenu(BigDecimal totalAmountOfCurrentGame) {
        System.out.println("Total amount of current win: " + totalAmountOfCurrentGame);
    }

    public GameCharacteristics endMenu(GameCharacteristics game) {
        String userResponse;
        System.out.println("Balance: " + game.getBalance());
        while (true) {
            System.out.println("If you  want to continue the game(Type 'y')" +
                    "\nIf you want to finish your game(Type 'n'):");

            userResponse = input.nextLine();

            if (game.getBalance().compareTo(MIN_BET_AMOUNT) < 0) {
                System.out.println("You do not have enough balance to continue to play");
                System.exit(0);
            }

            if (userResponse.trim().equals("n")) {
                System.exit(0);
                return game;
            }
            if (userResponse.trim().equals("y")) {
                return game;

            }
        }
    }

    private static int intInput(int endRange) {
        boolean isInt = true;
        String strNumberOfLines = "";
        int intNumberOfLines = -1;

        while (isInt) {
            try {
                strNumberOfLines = input.nextLine();
                intNumberOfLines = Integer.parseInt(strNumberOfLines);
                if (intNumberOfLines < 1 || intNumberOfLines > endRange) {
                    System.out.printf("%s is not a valid number. Please enter a valid integer between %d and %d \n", strNumberOfLines, 1, endRange);
                    continue;
                }
                isInt = false;
            } catch (IllegalArgumentException e) {
                System.out.printf("%s is not a valid number. Please enter a valid integer between %d and %d \n", strNumberOfLines, 1, endRange);
            }
        }

        return intNumberOfLines;
    }

    private static BigDecimal bigDecimalInput(BigDecimal endRange) {
        boolean isInt = true;
        String strInput = "";
        BigDecimal betPerLine = new BigDecimal(0);
        String[] arrToCheckBigDecimal;
        while (isInt) {
            try {
                strInput = input.nextLine();
                if (strInput.contains(",")) {
                    strInput = strInput.replace(",", ".");
                }
                arrToCheckBigDecimal = strInput.split("\\.");
                if (arrToCheckBigDecimal.length > 2) {
                    System.out.printf("%s is not valid Number. Please enter valid number. Example: 4.99\n", strInput);
                    continue;
                }
                if (arrToCheckBigDecimal.length > 1) {
                    if (arrToCheckBigDecimal[1].length() > 2) {
                        System.out.printf("%s is not a valid Number. You can have only two digits after the decimal point.\nPlease enter valid number.\nExample: 4.99\n", strInput);
                        continue;
                    }
                }
                betPerLine = new BigDecimal(strInput);
                if (betPerLine.compareTo(constants.Constants.MIN_BET_AMOUNT) < 0 || betPerLine.compareTo(endRange) > 0) {
                    System.out.printf("%s is not a valid number. Please enter a valid number between %.2f and %.2f \n", strInput, constants.Constants.MIN_BET_AMOUNT, endRange);
                    continue;
                }
                isInt = false;
            } catch (IllegalArgumentException e) {
                System.out.printf("%s is not a valid number. Please enter a valid number between %.2f and %.2f \n", strInput, constants.Constants.MIN_BET_AMOUNT, endRange);
            }
        }
        return betPerLine;
    }
}
