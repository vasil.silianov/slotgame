import algorithms.Util;
import play.Play;

import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        Util.loadProperties();

        Play newPlay = new Play();
        newPlay.startGame();
    }
}
