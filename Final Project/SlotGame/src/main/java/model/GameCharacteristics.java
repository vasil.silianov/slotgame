package model;

import java.math.BigDecimal;

public class GameCharacteristics {
    private BigDecimal balance;
    private BigDecimal betPerLine;
    private BigDecimal totalBet;
    private int numberOfLinesToBet;

    //setters and getter

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBetPerLine() {
        return betPerLine;
    }

    public void setBetPerLine(BigDecimal betPerLine) {
        this.betPerLine = betPerLine;
    }

    public BigDecimal getTotalBet() {
        return totalBet;
    }

    public void setTotalBet(BigDecimal totalBet) {
        this.totalBet = totalBet;
    }

    public int getNumberOfLinesToBet() {
        return numberOfLinesToBet;
    }

    public void setNumberOfLinesToBet(int numberOfLinesToBet) {
        this.numberOfLinesToBet = numberOfLinesToBet;
    }
}
