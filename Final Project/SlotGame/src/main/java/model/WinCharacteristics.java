package model;

import java.math.BigDecimal;

public class WinCharacteristics {
    private int winningLine;
    private int winningElement;
    private int winningStreak;
    private int numberOfStartingWildCards;
    private BigDecimal winningAmount = new BigDecimal(0);


    public int getNumberOfStartingWildCards() {
        return numberOfStartingWildCards;
    }

    public void setNumberOfStartingWildCards(int numberOfStartingWildCards) {
        this.numberOfStartingWildCards = numberOfStartingWildCards;
    }

    public int getWinningLine() {
        return winningLine;
    }

    public void setWinningLine(int winningLine) {
        this.winningLine = winningLine;
    }

    public int getWinningElement() {
        return winningElement;
    }

    public void setWinningElement(int winningElement) {
        this.winningElement = winningElement;
    }

    public int getWinningStreak() {
        return winningStreak;
    }

    public void setWinningStreak(int winningStreak) {
        this.winningStreak = winningStreak;
    }

    public BigDecimal getWinningAmount() {
        return winningAmount;
    }

    public void setWinningAmount(BigDecimal winningAmount) {
        this.winningAmount = winningAmount;
    }
}
