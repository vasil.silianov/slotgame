package config;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFromFile {
    public ArrayList<ArrayList<Integer>> read(String path) {
        URL resource = getClass().getClassLoader().getResource(path);
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        int i = 0;
        try {
            File file = new File(resource.toURI());
            Scanner fileReader = new Scanner(file);

            while (fileReader.hasNextLine()) {
                String data = fileReader.nextLine();
                data = data.replace("[", "");
                data = data.replace("]", "");
                String[] tempArray = data.replace(" ", ",").split(",");
                result.add(new ArrayList<>());
                for (int j = 0; j < tempArray.length; j++) {
                    result.get(i).add(Integer.parseInt(tempArray[j]));
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
