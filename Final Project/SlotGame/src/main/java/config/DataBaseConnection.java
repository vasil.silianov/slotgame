package config;

import java.math.BigDecimal;
import java.sql.*;

public class DataBaseConnection {
    private String url;
    private String user;
    private String password;

    public DataBaseConnection(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void saveWinInTheDB( int numberOfLinesToBet, BigDecimal betPerLine
            , BigDecimal totalWiningAmount, BigDecimal balance, int[] generatedReelsPositions) {

        String query = "Insert into game_statistics(number_of_lines_to_bet,bet_per_line,total_winning_amount,balance," +
                "reel_index_1,reel_index_2,reel_index_3,reel_index_4,reel_index_5)" +
                " values(" + numberOfLinesToBet +
                "," + betPerLine +
                "," + totalWiningAmount +
                "," + balance +
                "," + generatedReelsPositions[0] +
                "," + generatedReelsPositions[1] +
                "," + generatedReelsPositions[2] +
                "," + generatedReelsPositions[3] +
                "," + generatedReelsPositions[4] +
                ") ";
        executeCRUDQuery(query);
    }
    public boolean executeCRUDQuery( String query) {
        try(Connection connection =connectToDB()) {
            Statement statement = connection.createStatement();
            return    statement.execute(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private Connection connectToDB() {
        Connection connection;// todo read upon best practices more specifically connection pool
        try {
            Class.forName(System.getProperty("db.driver"));
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }




}
