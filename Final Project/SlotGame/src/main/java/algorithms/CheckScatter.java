package algorithms;

import config.ReadFromFile;
import model.WinCharacteristics;

import java.math.BigDecimal;
import java.util.ArrayList;

import static constants.Constants.*;

public class CheckScatter {
    private ArrayList<ArrayList<Integer>> payTable;


    public CheckScatter(ArrayList<ArrayList<Integer>> payTable) {
        this.payTable = payTable;
    }

    /**
     * @param slotScreen - Is two-dimensional array that represents the current slot screen of the game.
     * @param totalBet -Is variable that is calculated on the base of two other variables, betPerLine and numberOfLinesToBet.
     *                 totalBet = betPerLine * numberOfLinesToBet
     * @return Object of type WinCharacteristics in which are stored the properties of a scatter
     * win(if we have a win else returns object with empty properties).
     * Check is there a win, only if there is a win,
     * calculates the win of scatters, by multiplying totalBet and the payTable.get(7).get(numberOfScatters-DISPOSITION_OF_PAY_TABLE_ARRAY)
     *  scatterWin.setWinningAmount(totalBet.multiply(new BigDecimal(payTable.get(7).get(numberOfScatters-DISPOSITION_OF_PAY_TABLE_ARRAY))));
     *  Sets the number of scatters: scatterWin.setWinningStreak(numberOfScatters);
     *  Sets the winning element: scatterWin.setWinningElement(SCATTER);
     *  Sets the winning line as -1, which is a marker in flow to recognize that this is scatter win.
     */
    public WinCharacteristics calculateScatterWins(int[][] slotScreen, BigDecimal totalBet){
        int numberOfScatters = numberOfScatters(slotScreen);
        WinCharacteristics scatterWin = new WinCharacteristics();
        if (numberOfScatters<3){
            return scatterWin;
        }
        scatterWin.setWinningAmount(totalBet.multiply(new BigDecimal(payTable.get(7).get(numberOfScatters-DISPOSITION_OF_PAY_TABLE_ARRAY))));
        scatterWin.setWinningStreak(numberOfScatters);
        scatterWin.setWinningElement(SCATTER);
        scatterWin.setWinningLine(-1);
        return scatterWin;
    }


    /**
     *
     * @param slotScreen - Is two-dimensional array that represents the current slot screen of the game.
     * @constant SCATTER - This constant is used to compare to each element in the two-dimensional array slotScreen.
     * @return one integer, this integer represents the number of scatters that the algorithm has found in the current
     * slot screen.
     * The logic algorithm is: loop through all positions of the two-dimensional array and check every single element is
     * it equal to the variable SCATTER
     */
    private int numberOfScatters(int[][] slotScreen){
        int numberOfScatters =0;

        for (int i = 0; i <slotScreen.length ; i++) {
            for (int j = 0; j <slotScreen[i].length ; j++) {
                if (slotScreen[i][j] == SCATTER){
                    numberOfScatters++;
                }
            }
        }
        return numberOfScatters;
    }

}
