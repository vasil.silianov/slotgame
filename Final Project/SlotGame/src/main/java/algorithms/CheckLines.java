package algorithms;

import config.ReadFromFile;
import model.WinCharacteristics;

import java.math.BigDecimal;
import java.util.ArrayList;

import static constants.Constants.*;

public class CheckLines {

    ArrayList<ArrayList<Integer>> winningLines;//= lineConfig.lineConfig(filePath);
    ArrayList<ArrayList<Integer>> payTable;//= cardsPaytableConfig.read(path);

    public CheckLines(ArrayList<ArrayList<Integer>> cardsPaytableConfig, ArrayList<ArrayList<Integer>> winningLinesConfig) {

        this.payTable = cardsPaytableConfig;
        this.winningLines = winningLinesConfig;

    }

    /**
     *
     * @param betPerLine - The amount the player is betting on each line
     * @param currentSlotScreen - Two-dimensional array which represents the current slot screen.
     * @param numberOfLinesToCheck - This variable represents the number of lines the player has bet on and the algorithm has to check.
     * @return - Returns ArrayList of WinCharacteristics where on each object has been set the winning amount.
     * The logic of the method:
     * 1. We create a variable of type ArrayList<><WinCharacteristics> allWinningLines, and we initialize it with the result of method findWinningLinesFromCurrentSlotScreen(currentSlotScreen, numberOfLinesToCheck).
     * 2. We, create variable multiplier and then loop through allWinningLines.
     * 2.1. We create variable winElementMultiplier, and we initialize it with the value of the multiplier of the winning element of the current win.
     * After that we are assigning to multiplier the value of winElementMultiplier. We are doing this because we have to determine is there a bigger win or  same amount win but with  bigger card.
     * 2.2. We check the number starting wild cards if it is bigger or equal to 2 then we create a variable wildElementMultiplier,
     * and we assign to this variable the multiplier of for the wild card.
     * 2.3. Then we compare the multipliers, if the wildElementMultiplier>=winElementMultiplier this means that the win should be displayed and multiplied as WILD card win.
     * Then we set the winning element as WILDCARD, win streak as win.getNumberOfStartingWildCards() and multiplier as wildElementMultiplier.
     * 3. We calculate the winning amount betPerLine*multiplier, and we set the value in win.setWinningAmount.
     * 4. After this is done for all elements in the allWinningLines array, we return the array.
     */
    public ArrayList<WinCharacteristics> calculateBalanceForEachWinningLine(BigDecimal betPerLine, int[][] currentSlotScreen, int numberOfLinesToCheck) {
        ArrayList<WinCharacteristics> allWinningLines = findWinningLinesFromCurrentSlotScreen(currentSlotScreen, numberOfLinesToCheck);
        int multiplier;
        for (WinCharacteristics win : allWinningLines) {
            int winElementMultiplier = payTable.get(win.getWinningElement()).get(win.getWinningStreak() - DISPOSITION_OF_PAY_TABLE_ARRAY);
            multiplier = winElementMultiplier;
            if (win.getNumberOfStartingWildCards() >= MIN_WIN_STREAK_WILD_CARD) {
                int wildElementMultiplier = payTable.get(WILDCARD).get(win.getNumberOfStartingWildCards() - DISPOSITION_OF_PAY_TABLE_ARRAY);
                if (winElementMultiplier <= wildElementMultiplier) {
                    win.setWinningElement(WILDCARD);
                    win.setWinningStreak(win.getNumberOfStartingWildCards());
                    multiplier = wildElementMultiplier;
                }
            }
            win.setWinningAmount(betPerLine
                    .multiply(BigDecimal
                            .valueOf(multiplier)));
        }
        return allWinningLines;
    }

    /**
     * @param allWinCalculatedValues - ArrayList of WinCharacteristics where are filtered only the winning cases and has calculated the winning amount for each win.
     * @return - Returns BigDecimal which represents the total amount of all wins.
     * The logic of the method:
     * 1. Create variable: BigDecimal totalAmount = new BigDecimal(0);
     * 2. Loop through allWinCalculatedValues and adds the amount of each win.
     * 3. Returns totalAmount.
     */
    public BigDecimal totalAmountOfLineWins(ArrayList<WinCharacteristics> allWinCalculatedValues) {
        BigDecimal totalAmount = new BigDecimal(0);
        for (WinCharacteristics currentWinAmount :
                allWinCalculatedValues) {
            totalAmount = totalAmount.add(currentWinAmount.getWinningAmount());
        }
        return totalAmount;
    }

    /**
     *
     * @param currentSlotScreen - This variable is an ArrayList that represents all the position that creates the line.
     *                          Values from 0 to 2 in each reel of the currentSlotScreen.
     * @param numberOfLinesToCheck - This variable represents the number of lines the player has bet on and the algorithm has to check.
     * @return - Returns ArrayList of WinCharacteristics where are filtered only the winning cases.
     * The logic of the method:
     * 1. We create variable allWins and currentWinLine:
     * ArrayList<WinCharacteristics> allWins = new ArrayList<>();
     * WinCharacteristics currentWinLine;
     * 2. We loop numberOfLinesToCheck times through winningLines, where winningLines is two-dimensional ArrayList of integer generated by lineConfig.txt
     * 2.1 First we check if currentWinLine.getWinningStreak() == 2 && currentWinLine.getWinningElement() == WILDCARD, if is true then we set the line as a win in allWins.
     * 2.2 Second we check if currentWinLine.getWinningStreak() < 3, if is true then we continue to the next line to check.
     * 2.3 In all other cases we have a winning line then we are setting the specific line after that we are adding it into allWins.
     * 3. We return allWins.
     */
    private ArrayList<WinCharacteristics> findWinningLinesFromCurrentSlotScreen(int[][] currentSlotScreen, int numberOfLinesToCheck) {
        ArrayList<WinCharacteristics> allWins = new ArrayList<>();
        WinCharacteristics currentWinLine;
        for (int i = 0; i < numberOfLinesToCheck; i++) {
            currentWinLine = checkLine(winningLines.get(i), currentSlotScreen);
            if (currentWinLine.getWinningStreak() == MIN_WIN_STREAK_WILD_CARD && currentWinLine.getWinningElement() == WILDCARD) {
                currentWinLine.setWinningLine(i);
                allWins.add(currentWinLine);
                continue;
            }
            if (currentWinLine.getWinningStreak() < MIN_WIN_STREAK_NORMAL_CARD) {
                continue;
            }
            currentWinLine.setWinningLine(i);
            allWins.add(currentWinLine);
        }
        return allWins;
    }

    /**
     * @param currentLine - This variable is an ArrayList that represents all the position that creates the line.
     *                    Values from 0 to 2 in each reel of the currentSlotScreen.
     * @param currentSlotScreen - Two-dimensional array which represents the current slot screen.
     * @return - Returns object of type WinCharacteristics.
     * The logic of the method:
     * 1. Create variables win, positionInReel, numberOfOccurrence and card.
     * 2. Initialize variable card = currentSlotScreen[currentLine.get(0)][0];(The first value of the current line).
     * 2.1. Check if card == SCATTER the execution of the method stops else continue the execution of the method.
     * 3. Initialize variable numberOfOccurrence = 1, because at this point we have one occurrence of the current card.
     * 4. We loop through the element of currentLine however we star from 1 not form 0 because we've already seen the first  element.
     * 4.1. We assign the value of currentLine.get(j) on the variable positionInReel: positionInReel = currentLine.get(j);.
     * This is dome mostly for readability.
     * 4.2. We check is  card equal to currentSlotScreen[positionInReel][j](is it the same element as variable card) or is it equal to WILDCARD.
     * If it is equal to either, then we increment the numberOfOccurrence.
     * 4.3 Else If card is equal to WILDCARD AND currentSlotScreen[positionInReel][j] is different from SCATTER: {else if (card == WILDCARD && currentSlotScreen[positionInReel][j] != SCATTER)},
     * then we set the value of numberOfOccurrence to the win variable to mark how many times we have encountered WILDCARD in the begging of the current line: {win.setNumberOfStartingWildCards(numberOfOccurrence);}.
     * 4.4 We also change the value of variable card to: card = currentSlotScreen[positionInReel][j];, because the value in position currentSlotScreen[positionInReel][j] is different but the streak continues.
     * 4.5 We increment the number of occurrence: numberOfOccurrence++;
     * 4.6 Else,  in all other cases we break because this line is not a winning line.
     * 5.Despite is it a winning line or not we set to the variable win the properties of card and numberOfOccurrence:
     *         win.setWinningElement(card);//
     *         win.setWinningStreak(numberOfOccurrence);
     * 6. We return the win variable
     */
    private WinCharacteristics checkLine(ArrayList<Integer> currentLine, int[][] currentSlotScreen) {
        WinCharacteristics win = new WinCharacteristics();
        int positionInReel;
        int numberOfOccurrence;

        int card = currentSlotScreen[currentLine.get(0)][0];
        if (card == SCATTER) {
            return win;
        }

        numberOfOccurrence = 1;
        for (int j = 1; j < currentLine.size(); j++) {
            positionInReel = currentLine.get(j);
            if (card == currentSlotScreen[positionInReel][j] || currentSlotScreen[positionInReel][j] == WILDCARD) { // first change WILDCARD
                numberOfOccurrence++;
            } else if (card == WILDCARD && currentSlotScreen[positionInReel][j] != SCATTER) {
                win.setNumberOfStartingWildCards(numberOfOccurrence);
                card = currentSlotScreen[positionInReel][j];
                numberOfOccurrence++;
            } else {
                break;
            }
        }

        win.setWinningElement(card);//
        win.setWinningStreak(numberOfOccurrence);
        return win;

    }


    /// testing
/* Закометирма от тук!
    public static void main(String[] args) {
//        Types[] types = Types.values();
//        System.out.println(types[0]);
//        System.out.println(types[2]);
//        System.out.println(types[1]);
//        System.out.println(types[6]);
//        System.out.println(types[0]);

//        HashMap<String,Integer> test = new HashMap<>();
//        System.out.println(test.size());


        int[][] arr = {{1, 1, 1, 1, 5},
                {7, 1, 0, 1, 5},
                {4, 7, 1, 7, 0}};

        int[][] arr2 = {{6, 6, 7, 1, 5},
                {7, 5, 7, 1, 5},
                {4, 7, 1, 7, 0}};

        int[][] arr3 = {{1, 6, 1, 1, 5},
                {7, 1, 0, 1, 5},
                {4, 7, 1, 7, 0}};
        int[][] arr4 = {{1, 4, 1, 4, 5},
                {4, 3, 0, 1, 4},
                {2, 0, 4, 7, 0}};

        ReadFromFile fileReader = new ReadFromFile();
        ArrayList<ArrayList<Integer>> lineConfig = fileReader.read(LINE_CONFIG_PATH);
        ArrayList<ArrayList<Integer>> payTable = fileReader.read(PAY_TABLE_CONFIG_PATH);
//        CardsPaytableConfig cardsPaytableConfig = new CardsPaytableConfigImpl(fileReader);
        CheckLines checkLines = new CheckLines(payTable, lineConfig);

        ArrayList<WinCharacteristics> wins = checkLines.calculateBalanceForEachWinningLine(new BigDecimal(10), arr2, 20);

        for (WinCharacteristics w : wins) {
            System.out.println("Amount " + w.getWinningAmount() + " | Element " + w.getWinningElement() + " | Streak " + w.getWinningStreak() + " | Line " + w.getWinningLine());
        }
        /*
            public CheckLines(ReadFromFile lineConfig, String filePathOfLineConfig,
             String filePathOfPayTableConfig, ReadFromFile cardsPaytableConfig) {

        this.cardsPaytableConfig = cardsPaytableConfig;
        this.winningLines = lineConfig.read(filePathOfLineConfig);
        this.payTable = cardsPaytableConfig.read(filePathOfPayTableConfig);
    }




//        ArrayList<Map<TypesOfElements, Integer>> test1 = checkLines.winningLines(arr, 4);

//        for (Map<String, Integer> m :
//                test1) {
//            System.out.println(m.values());
//
//        }

//        System.out.println(test1.get(0).get(TypesOfElements.ZERO));
//        System.out.println(test1.get(0).get(TypesOfElements.ONE));
//        System.out.println(test1.get(0).get(TypesOfElements.TWO));
//        System.out.println(test1.get(0).get(TypesOfElements.THREE));
//        System.out.println(test1.get(0).get(TypesOfElements.FOUR));
//        System.out.println(test1.get(0).get(TypesOfElements.FIVE));
//        System.out.println(test1.get(0).get(TypesOfElements.SIX));

        System.out.println("#####################################");
//        ArrayList<BigDecimal> test3 = checkLines.calculateBalanceForEachWinningLine(new BigDecimal(5), test1);
//        for (BigDecimal b :
//                test3) {
//            System.out.println(b);
//        }
//        System.out.println(checkLines.totalAmountOfLineWins(test3));

    }
*/

}

