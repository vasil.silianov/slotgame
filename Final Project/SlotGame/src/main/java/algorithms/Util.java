package algorithms;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

import static constants.Constants.NUMBER_OF_SLOT_SCREEN_ROWS;


public class Util {

    private static Random random = new Random();


    public static void loadProperties() {
        Properties properties = new Properties();
        try (InputStream stream = Util.class.getClassLoader().getResourceAsStream("game.properties")) {
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        properties.putAll(System.getProperties());
        System.setProperties(properties);

    }

    /**
     * This method prints on the console "HI" before the actual game to begin.
     */
    public static void printHi() {
        char[][] arrHi = {
                {'H', '0', 'H', '0', 'O'},
                {'H', 'H', 'H', '0', 'I'},
                {'H', '0', 'H', '0', 'I'}
        };

        for (int i = 0; i < arrHi.length; i++) {
            for (int j = 0; j < arrHi[i].length; j++) {
                System.out.print(arrHi[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * @param currentBalance - This variable represents the current balance the player has.
     * @param numberOfLines  - This variable represents the number of lines the player has bet on.
     * @return - Returns maximum available bet base on the number of line the player wants to bet.
     * <p>
     * The method returns the rounded down value of the division of currentBalance and numberOfLines
     */
    public static BigDecimal maxAvailableBetPerLine(BigDecimal currentBalance, int numberOfLines) {
        return currentBalance.divide(new BigDecimal(numberOfLines), 2, RoundingMode.HALF_DOWN);
    }

    /**
     * @param arr - two-dimensional array of integers(int).
     *            This method prints on the console any two-dimensional array of integers.
     */
    public static void print2DIntArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * @param allReels - This variable represents an ArrayList of ArrayLists and contains reel information from file reelConfig.txt
     * @return - Returns array of random generated integers,
     * where each integer represents the begging of sub array in the corresponding reel with length = 3.
     * The logic of the method is:
     * 1. Creates an array of integers: int[] resultRandomNumbers = new int[allReels.size()];
     * 2. Loops through the variable allReels.
     * 2.1. for each real generates a random number in the range from 0 to allReels.get(i).size()-> the size of the current reel.
     * This number represents begging position in the allReel array: resultRandomNumbers[i] = random.nextInt(allReels.get(i).size());
     * 3. Returns array resultRandomNumbers
     */
    public static int[] returnAllRandomNumbers(ArrayList<ArrayList<Integer>> allReels) {

        int[] resultRandomNumbers = new int[allReels.size()];

        for (int i = 0; i < allReels.size(); i++) {

            resultRandomNumbers[i] = random.nextInt(allReels.get(i).size());
        }

        return resultRandomNumbers;
    }

    /**
     * @param allReels               - This variable represents an ArrayList of ArrayLists and contains reel information from file reelConfig.txt
     * @param generatedRandomNumbers - Array of random generated integers,
     *                               where each integer represents the begging of sub array in the corresponding reel with length = 3.
     * @return - Returns two-dimensional array which represents the current slot screen.
     * The logic of the method:
     * 1. Creates two-dimensional array slotScreen with length 3 for number sub array( which represent rows)
     * and generatedRandomNumber.length(5 in our case) number of elements in each sub array.
     * 2. Loops through generatedRandomNumbers
     * 2.1. Gets the next to elements in current reel array for each integer from generatedRandomNumbers.
     * 3. Returns initialized array slotScreen, which represents the slot screen which will be checked for wins.
     */
    public static int[][] currentSlotScreen(ArrayList<ArrayList<Integer>> allReels, int[] generatedRandomNumbers) {
        int[][] slotScreen = new int[NUMBER_OF_SLOT_SCREEN_ROWS][generatedRandomNumbers.length];

        for (int i = 0; i < generatedRandomNumbers.length; i++) {
            for (int j = 0; j < NUMBER_OF_SLOT_SCREEN_ROWS; j++) {
                slotScreen[j][i] = allReels.get(i).get((generatedRandomNumbers[i] + j) % allReels.get(i).size());
            }
        }
        return slotScreen;
    }

}
