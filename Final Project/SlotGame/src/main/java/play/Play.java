package play;

import algorithms.CheckLines;
import algorithms.CheckScatter;
import algorithms.Util;
import config.DataBaseConnection;
import config.ReadFromFile;
import menu.Menu;
import menu.impl.MenuImpl;
import model.GameCharacteristics;
import model.WinCharacteristics;

import java.math.BigDecimal;
import java.util.ArrayList;

import static constants.Constants.*;
import static constants.Constants.START_BALANCE;

public class Play {

    private GameCharacteristics game;
    private BigDecimal currentBalance;
    private ArrayList<ArrayList<Integer>> allReels;
    private ArrayList<ArrayList<Integer>> allLines;
    private ArrayList<ArrayList<Integer>> payTable;
    //    private GetRandomNumberInReel generateSlotScreen;
    private CheckLines checkLines;
    private Menu menu;
    private CheckScatter checkScatter;

    private DataBaseConnection dataBaseConnection;


    public Play() {
        game = new GameCharacteristics();
        ReadFromFile fileReader = new ReadFromFile();
        allReels = fileReader.read(REEL_CONFIG_PATH);
        allLines = fileReader.read(LINE_CONFIG_PATH);
        payTable = fileReader.read(PAY_TABLE_CONFIG_PATH);
        checkLines = new CheckLines(payTable, allLines);
        checkScatter = new CheckScatter(payTable);
        menu = new MenuImpl();
        dataBaseConnection = new DataBaseConnection(System.getProperty("db.url"),
                System.getProperty("db.user"), System.getProperty("db.password"));
        currentBalance = START_BALANCE;
    }

    public void startGame() {
        BigDecimal maxBetPerLineAvailable;
        int[][] currentSlotScreen;
        int[] generatedReelsPositions;
        ArrayList<WinCharacteristics> allWins;
        WinCharacteristics scatterWin;
        BigDecimal totalAmountOfCurrentGame;
        Util.printHi();

        while (true) {
            maxBetPerLineAvailable = Util.maxAvailableBetPerLine(currentBalance, MAX_AVAILABLE_LINES);
            game = menu.startMenu(currentBalance, MAX_AVAILABLE_LINES, maxBetPerLineAvailable);
            generatedReelsPositions = Util.returnAllRandomNumbers(allReels);
            currentSlotScreen = Util.currentSlotScreen(allReels, generatedReelsPositions);
            Util.print2DIntArr(currentSlotScreen);
            allWins = checkLines.calculateBalanceForEachWinningLine(game.getBetPerLine(), currentSlotScreen, game.getNumberOfLinesToBet());
            scatterWin = checkScatter.calculateScatterWins(currentSlotScreen, game.getTotalBet());
            totalAmountOfCurrentGame = checkLines.totalAmountOfLineWins(allWins).add(scatterWin.getWinningAmount());
            menu.winsMenu(allWins, scatterWin);
            if (totalAmountOfCurrentGame.compareTo(new BigDecimal(0)) == 0) {
                menu.noWinsMenu();
                currentBalance = menu.endMenu(game).getBalance();
            } else {
                dataBaseConnection.saveWinInTheDB(game.getNumberOfLinesToBet(), game.getBetPerLine(), totalAmountOfCurrentGame,
                        currentBalance, generatedReelsPositions);
                menu.totalAmountMenu(totalAmountOfCurrentGame);
                game.setBalance(game.getBalance().add(totalAmountOfCurrentGame));
                currentBalance = menu.endMenu(game).getBalance();
            }
        }
    }
}
